package com.hsnozan.foursquare.util;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import com.hsnozan.foursquare.R;

/**
 * Created by ozanal on 2019-05-29
 */
public class FSFrameLayout extends FrameLayout {

    private ProgressBar progressBar;
    private volatile boolean consumeTouches = false;

    public FSFrameLayout(@NonNull Context context) {
        super(context);

        init();
    }

    private void init() {
        setClickable(true);

        if (getBackground() == null) {
            setBackgroundColor(ContextCompat.getColor(getContext(), R.color.colorNavigation));
        }
    }

    public FSFrameLayout(@NonNull Context context,
                          @Nullable AttributeSet attrs,
                          int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        init();
    }

    public FSFrameLayout(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        init();
    }

    @Override
    public void setPressed(boolean pressed) {

    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return isConsumeTouches();
    }

    private boolean isConsumeTouches() {
        return consumeTouches || (progressBar != null
                && progressBar.getVisibility() == View.VISIBLE);
    }

    public void showProgress() {
        if (progressBar == null) {
            progressBar =
                    new ProgressBar(getContext(),
                            null,
                            android.R.attr.progressBarStyle);
            progressBar.getIndeterminateDrawable()
                    .setColorFilter(getResources().getColor(R.color.colorPrimary),
                            android.graphics.PorterDuff.Mode.MULTIPLY);

            LayoutParams mParams =
                    new LayoutParams(
                            ViewGroup.LayoutParams.WRAP_CONTENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT);
            mParams.gravity = Gravity.CENTER;

            addView(progressBar, mParams);
        }

        progressBar.setVisibility(View.VISIBLE);
        consumeTouches = true;
    }

    public void hideProgress() {
        if (progressBar != null && progressBar.getVisibility() == View.VISIBLE) {
            progressBar.setVisibility(View.GONE);
        }

        consumeTouches = false;
    }

    public void setConsumeTouches(boolean consumeTouches) {
        this.consumeTouches = consumeTouches;
    }
}
