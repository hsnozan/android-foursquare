package com.hsnozan.foursquare.scenes.detail;


import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.hsnozan.foursquare.R;
import com.hsnozan.foursquare.scenes.detail.adapter.VenueTipAdapter;
import com.hsnozan.foursquare.scenes.home.model.VenueDetailModel;
import com.hsnozan.foursquare.scenes.login.model.VenueModel;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class VenueFragment extends DialogFragment implements OnMapReadyCallback {

    @BindView(R.id.map_view)
    MapView mapView;
    @BindView(R.id.image_venue)
    AppCompatImageView imageView;
    @BindView(R.id.venue_name)
    AppCompatTextView venueName;
    @BindView(R.id.recycler_view_venue_tips)
    RecyclerView tipsRecyclerView;

    private VenueDetailModel venueDetailModel;
    private VenueModel venueModel;
    private LatLng latLng;
    private VenueTipAdapter adapter;

    public VenueFragment() {
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.fragment_venue, null);
        builder.setView(view);
        Dialog dialog = builder.create();
        ButterKnife.bind(this, view);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);
        setUpView();
        dialog.setCanceledOnTouchOutside(true);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));
        return dialog;
    }

    private void setUpView() {
        Glide.with(getContext()).load(venueDetailModel.imageUrl).into(imageView);
        venueName.setText(venueModel.venueNameModel);
        adapter = new VenueTipAdapter();
        adapter.setList(venueDetailModel.tipTextArray);
        tipsRecyclerView.setAdapter(adapter);
        tipsRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    @Override
    public void onMapReady(GoogleMap map) {
        LatLngBounds.Builder latLngBounds = LatLngBounds.builder();
        if (latLng != null) {
            latLngBounds.include(latLng);
            map.addMarker(new MarkerOptions()
                    .position(latLng)
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_CYAN))
                    .draggable(false).visible(true));
        }
    }

    public void setModels(VenueDetailModel venueDetailModel, VenueModel venueModel) {
        this.venueDetailModel = venueDetailModel;
        this.venueModel = venueModel;
        if (venueModel.venueLat != null && venueModel.venueLon != null) {
            latLng = new LatLng(venueModel.venueLat, venueModel.venueLon);
        }

    }
}
