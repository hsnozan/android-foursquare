package com.hsnozan.foursquare.scenes.home;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.hsnozan.foursquare.R;
import com.hsnozan.foursquare.scenes.detail.VenueFragment;
import com.hsnozan.foursquare.scenes.home.adapter.HomeAdapter;
import com.hsnozan.foursquare.scenes.home.listener.ItemClickListener;
import com.hsnozan.foursquare.scenes.home.model.VenueDetailModel;
import com.hsnozan.foursquare.scenes.login.LoginActivity;
import com.hsnozan.foursquare.scenes.login.model.VenueModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment implements ItemClickListener {

    private static final String VENUE_MODEL = "VENUE_MODEL";
    private static final String CLIENT_ID = "?client_id=IUYS05B222U4IL0DT1XT5D4ZDAKEC5ZRRHBYIWM1DBOWB3VO&client_secret=AGQVA5HZUCI2LH33KYO4QN312BNTEMIWIYBGVICPXHGURLMT&v=20191405";

    @BindView(R.id.recylerview_home)
    RecyclerView homeRecyclerView;

    private HomeAdapter adapter;
    private VenueModel venueModel;

    public static HomeFragment newInstance(ArrayList<VenueModel> venueModelArrayList) {
        HomeFragment homeFragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(VENUE_MODEL, venueModelArrayList);
        homeFragment.setArguments(args);
        return homeFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, view);

        if (getArguments() != null && getArguments().getParcelableArrayList(VENUE_MODEL) != null) {
            ArrayList<VenueModel> venueModelArrayList = getArguments().getParcelableArrayList(VENUE_MODEL);
            setUpAdapter(venueModelArrayList);
        }
        return view;
    }

    private void setUpAdapter(ArrayList<VenueModel> venueModelArrayList) {
        adapter = new HomeAdapter();
        adapter.setListener(this);
        adapter.setVenueList(venueModelArrayList);
        homeRecyclerView.setAdapter(adapter);
        homeRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    @Override
    public void onItemClickListener(VenueModel venueModel) {
        this.venueModel = venueModel;
        getVenueDetailFromUrl(venueModel.venueId);
    }

    private void getVenueDetailFromUrl(String venueId) {
        String fullUrl = "https://api.foursquare.com/v2/venues/" + venueId + CLIENT_ID;
        StringRequest request = new StringRequest(fullUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject object = new JSONObject(response);
                    JSONObject responseObj = object.getJSONObject("response");
                    JSONObject venueJson = responseObj.getJSONObject("venue");
                    VenueDetailModel venueModel;
                    venueModel = new VenueDetailModel(venueJson);

                    startVenueDetail(venueModel);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("responsseee", error.toString());
            }
        });

        RequestQueue rQueue = Volley.newRequestQueue(getContext());
        rQueue.add(request);
    }

    private void startVenueDetail(VenueDetailModel venueDetailModel) {
        LoginActivity activity = (LoginActivity) getActivity();

        if (activity != null) {
            VenueFragment venueFragment = new VenueFragment();
            venueFragment.setModels(venueDetailModel, venueModel);
            venueFragment.show(activity.getSupportFragmentManager(), "");
        }
    }
}
