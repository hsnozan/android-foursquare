package com.hsnozan.foursquare.scenes.home.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hsnozan.foursquare.R;
import com.hsnozan.foursquare.scenes.home.HomeFragment;
import com.hsnozan.foursquare.scenes.home.listener.ItemClickListener;
import com.hsnozan.foursquare.scenes.login.model.VenueModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ozanal on 2019-05-29
 */
public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.ViewHolder> {

    private ArrayList<VenueModel> venueModels = new ArrayList<>();
    private ItemClickListener itemClickListener;

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_home_list_item, parent,
                        false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.bind(venueModels.get(i));
    }

    @Override
    public int getItemCount() {
        return venueModels.size();
    }

    public void setVenueList(ArrayList<VenueModel> venueModels) {
        this.venueModels = venueModels;
    }

    public void setListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.text_venue_name)
        AppCompatTextView venueName;
        @BindView(R.id.text_venue_address)
        AppCompatTextView venueAddress;
        @BindView(R.id.text_venue_city)
        AppCompatTextView venueCity;
        @BindView(R.id.text_venue_country)
        AppCompatTextView venueCountry;

        private VenueModel venueModel;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(this);
        }

        public void bind(VenueModel venueModel) {
            this.venueModel = venueModel;
            venueName.setText(venueModel.venueNameModel);
            venueAddress.setText(venueModel.venueAddress);
            venueCity.setText(venueModel.venueCity);
            venueCountry.setText(venueModel.venueCountry);
        }

        @Override
        public void onClick(View view) {
            if (itemClickListener != null) {
                itemClickListener.onItemClickListener(venueModel);
            }
        }
    }
}
