package com.hsnozan.foursquare.scenes.home.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by ozanal on 2019-05-29
 */
public class VenueDetailModel implements Parcelable {

    public String photoPrefix;
    public String photoSufix;
    public String imageUrl;
    public JSONObject bestPhoto;
    public JSONObject tipsModel;
    public JSONArray tipsGroupArray;
    public JSONArray tipsGroupItemsArray;
    public String tipText;
    public ArrayList<String> tipTextArray = new ArrayList<>();

    public VenueDetailModel(JSONObject jsonObject) {
        try {
            this.bestPhoto = jsonObject.getJSONObject("bestPhoto");
            this.photoPrefix = bestPhoto.getString("prefix");
            this.photoSufix = bestPhoto.getString("suffix");
            this.imageUrl = photoPrefix + "original" + photoSufix;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            this.tipsModel = jsonObject.getJSONObject("tips");
            this.tipsGroupArray = tipsModel.getJSONArray("groups");

            for (int i = 0; i < tipsGroupArray.length(); i++) {
                JSONObject groupArrayObj = tipsGroupArray.getJSONObject(i);
                this.tipsGroupItemsArray = groupArrayObj.getJSONArray("items");
                for (int j = 0; j < tipsGroupItemsArray.length(); j++) {
                    JSONObject itemObj = tipsGroupItemsArray.getJSONObject(j);
                    this.tipText = itemObj.getString("text");
                    tipTextArray.add(tipText);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    protected VenueDetailModel(Parcel in) {
        photoPrefix = in.readString();
        photoSufix = in.readString();
        imageUrl = in.readString();
        tipText = in.readString();
        tipTextArray = in.createStringArrayList();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(photoPrefix);
        dest.writeString(photoSufix);
        dest.writeString(imageUrl);
        dest.writeString(tipText);
        dest.writeStringList(tipTextArray);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<VenueDetailModel> CREATOR = new Creator<VenueDetailModel>() {
        @Override
        public VenueDetailModel createFromParcel(Parcel in) {
            return new VenueDetailModel(in);
        }

        @Override
        public VenueDetailModel[] newArray(int size) {
            return new VenueDetailModel[size];
        }
    };
}
