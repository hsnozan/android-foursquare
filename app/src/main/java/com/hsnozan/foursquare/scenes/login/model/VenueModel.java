package com.hsnozan.foursquare.scenes.login.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by ozanal on 2019-05-29
 */
public class VenueModel implements Parcelable {

    public String venueNameModel;
    public String venueId;
    public JSONArray venueCategoriesModel;
    public JSONObject venueLocationModel;
    public JSONObject iconModel;
    public String venueAddress;
    public String venueCity;
    public String venueCountry;
    public String venuePostalCode;
    public Double venueLat;
    public Double venueLon;
    public String prefix;
    public String suffix;
    public String imageUrl;

    public VenueModel(JSONObject jsonObject) {
        try {
            this.venueNameModel = jsonObject.getString("name");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            this.venueId = jsonObject.getString("id");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            this.venueCategoriesModel = jsonObject.getJSONArray("categories");
            if (venueCategoriesModel != null) {
                for (int i = 0; i < venueCategoriesModel.length(); i++) {
                    JSONObject categoriesObj = venueCategoriesModel.getJSONObject(i);
                    iconModel = categoriesObj.getJSONObject("icon");
                    prefix = iconModel.getString("prefix");
                    suffix = iconModel.getString("suffix");
                    imageUrl = prefix + "300x500" + suffix;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            this.venueLocationModel = jsonObject.getJSONObject("location");
            this.venueAddress = venueLocationModel.getString("address");
            this.venueLat = venueLocationModel.getDouble("lat");
            this.venueLon = venueLocationModel.getDouble("lng");
            this.venueCity = venueLocationModel.getString("city");
            this.venueCountry = venueLocationModel.getString("country");
            this.venuePostalCode = venueLocationModel.getString("postalCode");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    protected VenueModel(Parcel in) {
        venueNameModel = in.readString();
        venueId = in.readString();
        venueAddress = in.readString();
        venueCity = in.readString();
        venueCountry = in.readString();
        venuePostalCode = in.readString();
        if (in.readByte() == 0) {
            venueLat = null;
        } else {
            venueLat = in.readDouble();
        }
        if (in.readByte() == 0) {
            venueLon = null;
        } else {
            venueLon = in.readDouble();
        }
        prefix = in.readString();
        suffix = in.readString();
        imageUrl = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(venueNameModel);
        dest.writeString(venueId);
        dest.writeString(venueAddress);
        dest.writeString(venueCity);
        dest.writeString(venueCountry);
        dest.writeString(venuePostalCode);
        if (venueLat == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(venueLat);
        }
        if (venueLon == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(venueLon);
        }
        dest.writeString(prefix);
        dest.writeString(suffix);
        dest.writeString(imageUrl);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<VenueModel> CREATOR = new Creator<VenueModel>() {
        @Override
        public VenueModel createFromParcel(Parcel in) {
            return new VenueModel(in);
        }

        @Override
        public VenueModel[] newArray(int size) {
            return new VenueModel[size];
        }
    };
}
