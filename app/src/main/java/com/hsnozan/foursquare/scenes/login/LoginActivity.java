package com.hsnozan.foursquare.scenes.login;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.hsnozan.foursquare.R;
import com.hsnozan.foursquare.scenes.home.HomeFragment;
import com.hsnozan.foursquare.scenes.login.model.VenueModel;
import com.hsnozan.foursquare.service.UserLocation;
import com.hsnozan.foursquare.util.FSFrameLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pub.devrel.easypermissions.EasyPermissions;
import pub.devrel.easypermissions.PermissionRequest;

public class LoginActivity extends AppCompatActivity implements TextWatcher {

    private static final String VENUE_LIST = "VENUE_LIST";
    private static final int REQUEST_LOCATION_PERMISSION = 1;
    private static String BASE_URL = "https://api.foursquare.com/v2/venues/search?client_id=IUYS05B222U4IL0DT1XT5D4ZDAKEC5ZRRHBYIWM1DBOWB3VO&client_secret=AGQVA5HZUCI2LH33KYO4QN312BNTEMIWIYBGVICPXHGURLMT&v=20191405";

    @BindView(R.id.main_container)
    FSFrameLayout rootView;
    @BindView(R.id.edit_text_cafe_bar)
    AppCompatEditText venueEditText;
    @BindView(R.id.edit_text_location)
    AppCompatEditText locationEditText;
    @BindView(R.id.button_search)
    AppCompatButton searchButton;

    private String userLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        venueEditText.addTextChangedListener(this);
        requestUserLocation();
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        if (charSequence.toString().equals("") || charSequence.toString().length() < 3) {
            searchButton.setEnabled(false);
        } else {
            searchButton.setEnabled(true);
        }
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }

    @OnClick(R.id.button_search)
    public void onSearchClick() {
        readFromUrl();
    }

    private void readFromUrl() {
        rootView.showProgress();
        String venue = venueEditText.getText().toString();
        if (!locationEditText.getText().toString().equals("")) {
            userLocation = "&near=" + locationEditText.getText().toString();
        }

        String fullUrl = BASE_URL + userLocation + "&radius=500" + "&query=" + venue;
        StringRequest request = new StringRequest(fullUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject object = new JSONObject(response);
                    JSONObject responseObj = object.getJSONObject("response");
                    JSONArray venueJsonList = responseObj.getJSONArray("venues");
                    ArrayList<VenueModel> venueModelArrayList = new ArrayList<>();
                    VenueModel venueModel;

                    for (int i = 0; i < venueJsonList.length(); ++i) {
                        venueModel = new VenueModel(venueJsonList.getJSONObject(i));
                        venueModelArrayList.add(venueModel);
                    }

                    startHomeActivity(venueModelArrayList);
                } catch (JSONException e) {
                    rootView.hideProgress();
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("responsseee", error.toString());
            }
        });

        RequestQueue rQueue = Volley.newRequestQueue(this);
        rQueue.add(request);
    }

    private void startHomeActivity(ArrayList<VenueModel> venueModelArrayList) {
        rootView.hideProgress();
        HomeFragment homeFragment = HomeFragment.newInstance(venueModelArrayList);
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.add(getContainer(), homeFragment);
        transaction.addToBackStack(homeFragment.getTag());
        transaction.commitAllowingStateLoss();
    }

    public int getContainer() {
        return R.id.main_container;
    }

    public void requestUserLocation() {
        String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION};

        if (!hasPermissions(this, permissions)) {
            onLocationRequested();
        } else {
            getUserLocation();
        }
    }

    public boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission)
                        != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }

        return true;
    }

    public void onLocationRequested() {
        String[] perms = {Manifest.permission.ACCESS_FINE_LOCATION};

        if (EasyPermissions.hasPermissions(this, perms)) {
            getUserLocation();
        } else {
            EasyPermissions.requestPermissions(new PermissionRequest
                    .Builder(this, REQUEST_LOCATION_PERMISSION, perms)
                    .setRationale(R.string.text_need_location_access)
                    .setPositiveButtonText(R.string.text_ok)
                    .setNegativeButtonText(R.string.text_cancel)
                    .build());
        }
    }

    private void getUserLocation() {
        UserLocation.LocationResult locationResult = new UserLocation.LocationResult() {
            @Override
            public void gotLocation(final Location location) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        userLocation = "&ll=" + location.getLatitude() + "," + location.getLongitude();
                    }
                });
            }
        };

        UserLocation myLocation = new UserLocation();
        myLocation.getLocation(this, locationResult);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == REQUEST_LOCATION_PERMISSION) {
            EasyPermissions.onRequestPermissionsResult(requestCode, permissions,
                    grantResults, this);
            requestUserLocation();
        }
    }

}
