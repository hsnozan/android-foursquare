package com.hsnozan.foursquare.scenes.detail.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hsnozan.foursquare.R;
import com.hsnozan.foursquare.scenes.home.adapter.HomeAdapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ozanal on 2019-05-29
 */
public class VenueTipAdapter extends RecyclerView.Adapter<VenueTipAdapter.ViewHolder> {

    private ArrayList<String> tipTextArray = new ArrayList<>();
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_tip_list_item, parent,
                        false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.bind(tipTextArray.get(i));
    }

    @Override
    public int getItemCount() {
        if (tipTextArray != null) {
            return tipTextArray.size();
        } else {
            return 0;
        }
    }

    public void setList(ArrayList<String> tipTextArray) {
        this.tipTextArray = tipTextArray;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.text_tips)
        AppCompatTextView tipsText;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(String s) {
            tipsText.setText(s);
        }
    }
}
