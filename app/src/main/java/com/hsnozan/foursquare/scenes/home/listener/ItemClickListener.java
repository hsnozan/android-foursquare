package com.hsnozan.foursquare.scenes.home.listener;

import com.hsnozan.foursquare.scenes.login.model.VenueModel;

/**
 * Created by ozanal on 2019-05-29
 */
public interface ItemClickListener {

    void onItemClickListener(VenueModel venueModel);

}
